<?php
try {
// Configuration
  $dbhost = 'localhost';
  $dbname = 'empower_db';

  // open connection to MongoDB server
  $conn = new Mongo("mongodb://$dbhost");

  // access database
  $db = $conn->$dbname;

  // access collection
  $users = $db->users;

  // insert a new document
  $user = array(
    'first-name' => 'Your',
    'last-name' => 'Mom',
    'email' => 'is@home',
    'phone' => 'cooking'
  );
  $users->insert($user);
  echo 'Inserted document with ID: ' . $user['_id'];


  // disconnect from server
  $conn->close();
} catch (MongoConnectionException $e) {
  echo 'Error connecting to MongoDB server';
} catch (MongoException $e) {
  echo 'Error: ' , $e->getMessage();
}
?>
