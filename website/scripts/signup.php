<?php

try {
// Configuration
  $dbhost = 'localhost';
  $dbname = 'empower_db';

  // open connection to MongoDB server
  $conn = new Mongo("mongodb://$dbhost");

  // access database
  $db = $conn->$dbname;

  // access collection
  $users = $db->users;

  // insert a new document
  $user = array(
    'first-name' => $_POST['first-name'],
    'last-name' => $_POST['last-name'],
    'email' => $_POST['email'],
    'phone' => $_POST['phone']
  );
  if (!empty($user)) { 
      foreach ($user as $varname => $value) { 
        echo $varname . " : " . $value; 
      } 
  } 

  $users->insert($user);
  echo 'Inserted document with ID: ' . $user['_id'];


  // disconnect from server
  $conn->close();
} catch (MongoConnectionException $e) {
  die('Error connecting to MongoDB server');
} catch (MongoException $e) {
  die('Error: ' . $e->getMessage());
}
?>
