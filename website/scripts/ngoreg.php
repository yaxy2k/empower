<?php

try {
// Configuration
  $dbhost = 'localhost';
  $dbname = 'empower_db';

  // open connection to MongoDB server
  $conn = new Mongo("mongodb://$dbhost");

  // access database
  $db = $conn->$dbname;

  // access collection
  $orgs = $db->orgs;

  // insert a new document
  $org = array(
    'org-name' => $_POST['org-name'],
    'street-address' => $_POST['street-address'],
    'city' => $_POST['city'],
    'zipcode' => $_POST['zipcode']
  );

  $orgs->insert($org);
  echo 'Inserted document with ID: ' . $org['_id'];


  // disconnect from server
  $conn->close();
} catch (MongoConnectionException $e) {
  die('Error connecting to MongoDB server');
} catch (MongoException $e) {
  die('Error: ' . $e->getMessage());
}
?>
