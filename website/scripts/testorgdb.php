<?php

try {
  // Configuration
  $dbhost = 'localhost';
  $dbname = 'empower_db';

  // open connection to MongoDB server
  $conn = new Mongo("mongodb://$dbhost");

  // access database
  $db = $conn->$dbname;

  // access collection
  $orgs = $db->orgs;

  // execute query
  // retrieve all documents
  $cursor = $orgs->find();

  // iterate through the result set
  // print each document
  echo $cursor->count() . ' document(s) found. <br/>';  
  foreach ($cursor as $obj) {
    echo 'Organization Name: ' . $obj['org-name'] . '<br/>';
    echo 'Street Address: ' . $obj['street-address'] . '<br/>';
    echo 'City: ' . $obj['city'] . '<br/>';
    echo 'Zipcode: ' . $obj['zipcode'] . '<br/>';
    echo '<br/>';
  }

  // disconnect from server
  $conn->close();
} catch (MongoConnectionException $e) {
  echo 'MongoConnectionException: Error connecting to MongoDB server';
} catch (MongoException $e) {
  echo 'MongoException: Error: ' , $e->getMessage();
} catch (Exception $e) {
  echo 'Unknown Exception: Error: ' , $e->getMessage();
}
?>
