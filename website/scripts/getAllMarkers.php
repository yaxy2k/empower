<?php
try {
                        // Configuration
  $dbhost = 'localhost';
  $dbname = 'empower_db';

                        // open connection to MongoDB server
  $conn = new Mongo('mongodb://$dbhost');

                        // access database
  $db = $conn->$dbname;

                        // access collection
  $orgs = $db->orgs;

                        // execute query
                        // retrieve all documents
  $cursor = $orgs->find();

                        // iterate through the result set
                        // print the org-name and address separated by a ||
  

  echo json_encode($cursor);

                          // disconnect from server
  $conn->close();
} catch (MongoConnectionException $e) {
  echo 'MongoConnectionException: Error connecting to MongoDB server';
} catch (MongoException $e) {
  echo 'MongoException: Error: ' , $e->getMessage();
} catch (Exception $e) {
  echo 'Unknown Exception: Error: ' , $e->getMessage();
}
}
?>