<?php

try {
  // Configuration
  $dbhost = 'localhost';
  $dbname = 'empower_db';

  // open connection to MongoDB server
  $conn = new Mongo("mongodb://$dbhost");

  // access database
  $db = $conn->$dbname;

  // access collection
  $users = $db->users;

  // execute query
  // retrieve all documents
  $cursor = $users->find();

  // iterate through the result set
  // print each document
  echo $cursor->count() . ' document(s) found. <br/>';  
  foreach ($cursor as $obj) {
    echo 'First Name: ' . $obj['first-name'] . '<br/>';
    echo 'Last Name: ' . $obj['last-name'] . '<br/>';
    echo 'email: ' . $obj['email'] . '<br/>';
    echo 'phone: ' . $obj['phone'] . '<br/>';
    echo '<br/>';
  }

  // disconnect from server
  $conn->close();
} catch (MongoConnectionException $e) {
  echo 'MongoConnectionException: Error connecting to MongoDB server';
} catch (MongoException $e) {
  echo 'MongoException: Error: ' , $e->getMessage();
} catch (Exception $e) {
  echo 'Unknown Exception: Error: ' , $e->getMessage();
}
?>
