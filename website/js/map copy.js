
	var map, geocoder, markers = [], infowindow, line, ngoLocs, ngoAddrs, ngoTitles, iterator = 1;

    function initialize() {
        geocoder = new google.maps.Geocoder();
        var mapOptions = {
            center: new google.maps.LatLng(38, -97),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);

        infowindow = new google.maps.InfoWindow({
            maxWidth: 455
        });

        markers[0] = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position:new google.maps.LatLng(40.4939736, -74.433725),
            title: "em[power]"
        });
        markers[1] = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position:new google.maps.LatLng(40.975, -74.764),
            title: "American Purple Cross"
        });
        markers[2] = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position:new google.maps.LatLng(40.6947276, -74.80584),
            title: "Human Society for the Ethical Treatment of Bees"
        });
        markers[3] = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position:new google.maps.LatLng(40.6948373, -74),
            title: "Organization for the Advancement of Coding Robots"
        });
        markers[4] = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position:new google.maps.LatLng(40.57333, -74.472949),
            title: "Princeton Committee for the Advancement of Shirley Tilghman"
        });


        for (var i = 0; i < markers.length; i++) {
            popupDirections(markers[iterator]);
            iterator++;
        }

        $.getJSON("http://smart-ip.net/geoip-json?callback=?",
            function(data){
                map.panTo(new google.maps.LatLng(data.latitude, data.longitude));
                map.setZoom(10);
            }
            );
        // Places markers at the location of existing NGOs after parsing them for org-names
        // (ids) and locations.
        // infowindow.open(map, marker);

        //var json = response.responseText.evalJSON();

        // Tries to get all object-address pairs from php database and display them in map
        /*url = "scripts/getAllMarkers.php";
        $.ajax({
            type: "GET",
            url: url,
            data: {}, 
            success: function(data)
            {
                // For each json object inside data, gets the key org-name and address
                // and makes a marker out of it.
                for (x in data) {
                    ngoTitles[iterator] = data[x].org-name;
                    ngoAddrs[iterator] = data[x].address;
                    // encodes the address as a location and puts a marker on the map
                    codeAddress();
                    setTimeout(function() { 
                        addMarker();
                    }, i * 200);
                    iterator++;
                }
                iterator = 1;
            }
        });*/

    }

    function connectPoints(point1) {
        /*
        var points = [
            point1, 
            new google.maps.LatLng(X, Y)
        ];
        line = new google.maps.Polyline({
            path: points,
            strokeColor: "#FF0000",
            strokeOpacity: 1.0, 
            strokeWeight: 2
        });
        points.setMap(map);*/
    }

    // Gets the title from the marker and searches through the db using that title to find 
    // the corresponding information, and creates an infowindow with it.
    function popupDirections(marker) {
        google.maps.event.addListener(marker, 'click', function() {
            //url = "scripts/getMapMarkers.php";
            /*$.ajax({
                type: "POST",
                url: url,
                data: {title: marker.getTitle()}, 
                success: function(data)
                {
                    infowindow.setContent(data);
                }
            });*/
            infowindow.setContent(marker.title);
            infowindow.open(map, marker);
        });
    }

    function addMarker() {
        markers[iterator] = new google.maps.Marker({
            position: ngoLocs[iterator], 
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP, 
            title: ngoTitles[iterator]
        });
        popupDirections(markers[iterator]);
        iterator++;
    }

    // This function codes the address located in ngoAddrs and puts it into ngoLocs
    function codeAddress() {
        var address = ngoAddrs[iterator];
        geocoder.geocode(
            {'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    ngoLocs[iterator] = results[0].geometry.location;
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
    }

    $(document).ready(initialize);
    $(document).ready(function() {
        $.getJSON("http://smart-ip.net/geoip-json?callback=?",
            function(data){
                map.panTo(new google.maps.LatLng(data.latitude, data.longitude));
                map.setZoom(10);
            }
            );
    });
